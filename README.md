# ACTIVIDAD 4 - Animaciones

## Resumen del Proyecto
En esta actividad, se implementaron animaciones y transformaciones en una página web para lograr efectos visuales atractivos. El objetivo principal era mostrar imágenes con información adicional que se revelaría gradualmente al pasar el ratón sobre ellas. Se utilizaron transiciones de opacidad y escalado para crear una experiencia interactiva para el usuario.

## Requisitos de la Actividad
- Mostrar centrados en la página el título y el menú.
- El menú presenta el siguiente hover: cambia de color y subrayado.
- Aplicar las animaciones y transformaciones necesarias para lograr el siguiente efecto:
  - Al cargarse la página, solo se mostrarán los recuadros con las imágenes, ocultando el título, texto, enlace "Read more" y el fondo azul.
  - Al pasar el ratón sobre la imagen, debe ocurrir lo siguiente:
    - La imagen de fondo aumentará de tamaño, dando la impresión de acercamiento.
    - Se mostrará el fondo azul progresivamente con una opacidad para dejar ver la imagen que hay debajo.
    - El título aparecerá de manera progresiva desde una opacidad de 0 a 1.
    - El párrafo aparecerá de manera progresiva desde una opacidad de 0 a 1.
    - Se mostrará el enlace "Read more" de manera progresiva desde una opacidad de 0 a 1.
- El sitio web debe ser responsive, mostrando 3, 2 o 1 columna dependiendo del ancho del navegador.
- [Video de referencia](https://drive.google.com/file/d/1pfNQplOgJ4Hpadilfxlp36vGwEm0Z6hb/view)

## Contenido del Repositorio
- **img/**: Carpeta que contiene las imágenes utilizadas y capturas de cómo ha quedado la actividad.
- **Actividad-4-Lenguajes.pdf**: Documento PDF que describe los requisitos de la actividad.
- **index.html**: Plantilla para la página web de la actividad.
- **styles.css**: Archivo CSS que contiene estilos para la página web de la actividad.

## Capturas de la Actividad
Capturas de pantalla de la actividad finalizada:

- Pantalla Grande
![Pantalla Grande](img/large_screen.png)

- Pantalla Mediana
![Pantalla Mediana](img/medium_screen.png)

- Pantalla Pequeña
![Pantalla Pequeña](img/small_screen.png)

- Menú y Título
![Menú y Título](img/menu_and_title.png)

## Estado
[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=brightgreen)](https://gitlab.com/gusgonza/MP04-actividad-4/-/tree/main)
